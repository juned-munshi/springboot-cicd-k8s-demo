package com.cicddemo.demo.quiz.controller;

import com.cicddemo.demo.quiz.model.Quiz;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class QuizControllerTest {
    private static final String API_ROOT
            = "http://localhost:8080/api/quiz";

    private Quiz createRandomQuiz() {
        Quiz quiz = new Quiz();
        quiz.setTitle(randomAlphabetic(10));
        quiz.setDescription(randomAlphabetic(15));
        quiz.setThumbnail(randomAlphabetic(15));
        return quiz;
    }

    private String createBookAsUri(Quiz book) {
        Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(book)
                .post(API_ROOT);
        return API_ROOT + "/" + response.jsonPath().get("id");
    }
    @Test
    public void whenGetAllQuiz_thenOK() {
        Response response = RestAssured.get(API_ROOT);

        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }

}