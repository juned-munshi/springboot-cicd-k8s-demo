package com.cicddemo.demo.quiz.repository;

import com.cicddemo.demo.quiz.model.Quiz;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuizRepository extends CrudRepository<Quiz,Long> {
}

