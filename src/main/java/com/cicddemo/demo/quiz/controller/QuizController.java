package com.cicddemo.demo.quiz.controller;

import com.cicddemo.demo.quiz.model.Quiz;
import com.cicddemo.demo.quiz.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/quiz")
public class QuizController {

    @Autowired
    QuizService quizService;

    @GetMapping
    public Iterable<Quiz> getQuizList(){
        return quizService.getAllQuiz();
    }

    @GetMapping("/{id}")
    public Optional<Quiz> getQuizById(@PathVariable long id){
        return quizService.getQuizById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Quiz createQuiz(@RequestBody Quiz quiz){
        return quizService.saveQuiz(quiz);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
       quizService.deleteById(id);
    }

    @PutMapping("/{id}")
    public Quiz updateBook(@RequestBody Quiz q, @PathVariable long id) {
       return quizService.updateById(q,id);
    }
}

