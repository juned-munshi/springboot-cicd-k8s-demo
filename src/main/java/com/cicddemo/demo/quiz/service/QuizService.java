package com.cicddemo.demo.quiz.service;

import com.cicddemo.demo.quiz.model.Quiz;
import com.cicddemo.demo.quiz.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.QualifiedIdentifier;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class QuizService {

    @Autowired
    private QuizRepository quizRepository;

    public Iterable<Quiz> getAllQuiz(){
        return  quizRepository.findAll();
    }

    public Optional<Quiz> getQuizById(long id){
        return  quizRepository.findById(id);
    }

    public Quiz saveQuiz(Quiz q){
        return  quizRepository.save(q);
    }

    public void deleteById(long id){
        quizRepository.findById(id).orElseThrow();
        quizRepository.deleteById(id);
    }


    public Quiz updateById(Quiz q,long id)
    {
        if (q.getId() != id) {
            try {
                throw new Exception("Id mismatch");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        quizRepository.findById(id)
                .orElseThrow();
        return quizRepository.save(q);
    }


}
